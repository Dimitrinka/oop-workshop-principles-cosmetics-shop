package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;


public class ShampooImpl extends ProductBase implements Shampoo {
    private int milliliters;
    private UsageType everyDay;
    
    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType everyDay) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
      setEveryDay(everyDay);
    }

    private void setMilliliters(int milliliters) {
        if(milliliters < 0)
        {
            throw new IllegalArgumentException("Milliliters cannot be a negative number");
        }
        this.milliliters = milliliters;
    }

    private void setEveryDay(UsageType everyDay) {
        /*String inputToString= everyDay.toString();
        if(inputToString.equals("EveryDay"))
        {
            this.everyDay = UsageType.EVERYDAY;
        }
        else if(inputToString.equals("Medical"))
        {
            this.everyDay = UsageType.MEDICAL;
        }
        else{
            throw new IllegalArgumentException(String.format("Usage type can be %s or %s",UsageType.EVERYDAY,UsageType.MEDICAL));
        }*/
        this.everyDay = everyDay;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getBrand() {
       return super.getBrand();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public GenderType getGender() {
       return super.getGender();
    }

    @Override
    public String print() {
     /*   #Milliliters: 1000
 #Usage: EveryDay
                ret*/
        return  String.format("%s\t#Milliliters: %d\n\t#Usage: %s\n\t===",super.print(),getMilliliters(),getUsage());
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return everyDay;
    }
}

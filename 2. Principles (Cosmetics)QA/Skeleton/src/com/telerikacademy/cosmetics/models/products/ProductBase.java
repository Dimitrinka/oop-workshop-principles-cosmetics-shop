package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.UpperCase;


public class ProductBase implements Product {
    private static final int MIN_NAME_LENGTH = 3;
    public static final int MAX_NAME_LENGTH = 10;
    public static final int MIN_BRAND_NAME_LENGTH = 2;
    public static final int MAX_BRAND_NAME_LENGTH = 10;
    //Finish the class
    //What variables, what constants should you write here?
    //validate
    private String name;
    private String brand;
    private double price;
    private GenderType gender;
    
    ProductBase(String name, String brand, double price, GenderType gender) {
      //  throw new UnsupportedOperationException("Not implemented yet. ProductBase class");
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    private void setName(String name) {
        if(name == null)
        {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if(name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH)
        {
            throw new IllegalArgumentException(String.format("Name length be between %d and %d",MIN_NAME_LENGTH,MAX_NAME_LENGTH));
        }
        this.name = name;
    }

    private void setBrand(String brand) {
        if(brand == null)
        {
            throw new IllegalArgumentException("Brand cannot be null");
        }
        if(brand.length() < MIN_BRAND_NAME_LENGTH || brand.length() > MAX_BRAND_NAME_LENGTH)
        {
            throw new IllegalArgumentException(String.format("Brand name's length be between %d and %d",MIN_BRAND_NAME_LENGTH,MAX_BRAND_NAME_LENGTH));
        }
        this.brand = brand;
    }

    private void setPrice(double price) {
        if(price < 0)
        {
            throw  new IllegalArgumentException("Price cannot be negative");
        }
        this.price = price;
    }

    private void setGender(GenderType gender) {

      /* String genderToString= gender.toString();

        if(genderToString.equals("Men"))
        {
            this.gender = GenderType.MEN;
        }
        else if(genderToString.equals("Women")) {
            this.gender = GenderType.WOMEN;
        }
        else if(genderToString.equals("Unisex"))
        {
            this.gender = GenderType.UNISEX;
        }
        else{
            throw new IllegalArgumentException(String.format("Gender type can be %s, %s or %s",GenderType.MEN,GenderType.WOMEN,GenderType.UNISEX));
        }*/
        this.gender = gender;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
       return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
       return gender;
    }

    @Override
    public String print() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("#%s: %s\n\t#Price: $%.2f\n\t#Gender: %s\n", getName(),getBrand(),getPrice(),getGender()));
        return result.toString();
    }
}

package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;


import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {
    private  List<String> ingredients = new ArrayList<>();
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getBrand() {
        return  super.getBrand();
    }

    @Override
    public double getPrice() {
       return  super.getPrice();
    }

    @Override
    public GenderType getGender() {
       return  super.getGender();
    }

    @Override
    public String print() {
        StringBuilder builder = new StringBuilder();
        for (String product : ingredients) {

            builder.append(product);
        }
        return String.format("%s\t#Ingredients: [%s]\n\t===",super.print(),builder.toString());
    }
    private void setIngredients(List<String> ingredients) {
        if(ingredients == null)
        {
            throw new IllegalArgumentException("Ingredient cannot be null");
        }
        this.ingredients = ingredients;
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }
}

package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;


public class CreamImpl extends ProductBase implements Cream {
    private ScentType scent;
    
    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name,brand,price,gender);
        setScent(scent);

    }

    private void setScent(ScentType scent) {
        //Scent type can be "Rose", "Lavender" or "Vanilla"
        this.scent = scent;
    }

    @Override
    public String getName() {
       return super.getName();
    }

    @Override
    public String getBrand() {
        return super.getBrand();
    }

    @Override
    public double getPrice() {
        return  super.getPrice();
    }

    @Override
    public GenderType getGender() {
        return super.getGender();
    }

    @Override
    public String print() {
        return String.format("%s\t#Scent: %s\n\t===",super.print(),getScent());
    }

    @Override
    public ScentType getScent() {
        return scent;
    }
}

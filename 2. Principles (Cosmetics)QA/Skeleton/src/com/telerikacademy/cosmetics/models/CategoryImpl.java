package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;


import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    // CONSTANT ARE 2 AND 15, BECAUSE IN CLASS CATEGORY ARE THE SAME!!!!
    private static final int NAME_MIN_LENGTH = 2;
    private static final int NAME_MAX_LENGTH = 15;
    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        //validate
        setName(name);
        products = new ArrayList<>();
        //initialize the collection
    }

    private void setName(String name) {
        if(name == null)
        {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if(name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH)
        {
            throw new IllegalArgumentException(String.format("Name length be between %d and %d",NAME_MIN_LENGTH,NAME_MAX_LENGTH));
        }
        this.name = name;

    }

    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation!
        //-- We return a copy, because we want to save real list
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {

        if (product == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
        products.add(product);

    }
    
    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("Product not found in category.");
        }
        products.remove(product);

    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                    " #No product in this category", name);
        }
        //finish ProductBase class before implementing this method
        StringBuilder result = new StringBuilder();
        result.append(String.format("#Category: %s\n", name));

        for (Product product : products) {
            result.append(product.print());
        }

        return result.toString();



    }
    
}
